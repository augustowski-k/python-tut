from vector import Vector

vector1 = Vector([1,2,3])
print('vector1 => ', vector1)

vector2 = Vector([1,3,4,5])
print('vector2 => ', vector2)

vector3 = Vector([1,2,3])
print('vector3 => ', vector3)

print('vector1 == vector2: ', vector1 == vector2)
print('vector1 == vector3: ', vector1 == vector3)
print('vector2 == vector3: ', vector2 == vector3)

print('vector1 + vector2: ', vector1 + vector2)

print('vector1 - vector2: ', vector1 - vector2)
print('vector2 - vector1: ', vector2 - vector1)

print('vector1 * 2: ', vector1 * 2)
print('2 * vector1: ', 2 * vector1)

print('magnitude vector1: ', vector1.magnitude)
print('unit vector vector1: ', vector1.unit_vector, '\n')

print('Vector.parallel(Vector([-7.579, -7.88]), Vector([22.737, 23.64]))')
print(Vector.are_parallel(Vector([-7.579, -7.88]), Vector([22.737, 23.64])), '\n')
print('Vector.orthogonal(Vector([-7.579, -7.88]), Vector([22.737, 23.64]))')
print(Vector.are_orthogonal(Vector([-7.579, -7.88]), Vector([22.737, 23.64])), '\n')

print('Vector.parallel(Vector([-2.029, 9.79, 4.172]), Vector([-9.231, -6.639, -7.245]))')
print(Vector.are_parallel(Vector([-2.029, 9.79, 4.172]), Vector([-9.231, -6.639, -7.245])), '\n')
print('Vector.orthogonal(Vector([-2.029, 9.79, 4.172]), Vector([-9.231, -6.639, -7.245])))')
print(Vector.are_orthogonal(Vector([-2.029, 9.79, 4.172]), Vector([-9.231, -6.639, -7.245])), '\n')

print('print(Vector.parallel(Vector([-2.328, -7.284, -1.214]), Vector([-1.821, 1.072, -2.94])))')
print(Vector.are_parallel(Vector([-2.328, -7.284, -1.214]), Vector([-1.821, 1.072, -2.94])), '\n')
print('Vector.orthogonal(Vector([-2.328, -7.284, -1.214]), Vector([-1.821, 1.072, -2.94])))')
print(Vector.are_orthogonal(Vector([-2.328, -7.284, -1.214]), Vector([-1.821, 1.072, -2.94])), '\n')

print('print(Vector.parallel(Vector([2.118, 4.827]), Vector([0, 0])))')
print(Vector.are_parallel(Vector([2.118, 4.827]), Vector([0, 0])), '\n')
print('Vector.orthogonal(Vector([2.118, 4.827]), Vector([0, 0])))')
print(Vector.are_orthogonal(Vector([2.118, 4.827]), Vector([0, 0])), '\n')

print('Vector([3.039, 1.879]).project_onto(Vector([0.825, 2.036]))')
print(Vector([3.039, 1.879]).project_onto(Vector([0.825, 2.036])), '\n')
print('Vector([-9.88, -3.264, -8.159]).decompose_relative_to(-2.155, -9.353, -9.473)')
print(Vector([-9.88, -3.264, -8.159]).decompose_relative_to(Vector([-2.155, -9.353, -9.473])), '\n')
print('Vector([3.009, -6.172, 3.692, -2.51]).decompose_relative_to(Vector([6.404, -9.144, 2.759, 8.718]))')
print(Vector([3.009, -6.172, 3.692, -2.51]).decompose_relative_to(Vector([6.404, -9.144, 2.759, 8.718])), '\n')

print('Vector.cross_product(Vector([8.462, 7.893, -8.187]), Vector([6.984, -5.975, 4.778]))')
print(Vector.cross_product(Vector([8.462, 7.893, -8.187]), Vector([6.984, -5.975, 4.778])), '\n')
print('Vector.get_paralellogram_area(Vector([-8.987, -9.838, 5.031]), Vector([-4.268, -1.861, -8.866]))')
print(Vector.get_paralellogram_area(Vector([-8.987, -9.838, 5.031]), Vector([-4.268, -1.861, -8.866])), '\n')
print('Vector.get_paralellogram_area(Vector([1.5, 9.547, 3.691]), Vector([-6.007, 0.124, 5.772])) / 2.')
print(Vector.get_paralellogram_area(Vector([1.5, 9.547, 3.691]), Vector([-6.007, 0.124, 5.772])) / 2., '\n')