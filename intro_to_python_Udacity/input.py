username = input('What is your name?\n')

print('Hello {}'.format(username.title()))


age = int(float(input('And how old are you?\n')))

if age < 18:
    print('You have {} more years to go till you turn 18'.format(18 - age))
elif age == 18:
    print('Oh you\'re 18 already')
else:
    print('So you are saying that you turned 18 {} years ago?'.format(age - 18))