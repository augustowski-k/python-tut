names_string = input('Please enter names separated by commas: ')
names =  map(lambda s: s.strip(), names_string.split(','))
assignments_string = input('Please enter number of assignmetns separated by commas: ')
assignments =  map(lambda s: int(s.strip()), assignments_string.split(','))
grades_string = input('Please enter current grades separated by commas: ')
grades =  map(lambda s: int(s.strip()), grades_string.split(','))

message = "Hi {},\n\nThis is a reminder that you have {} assignments left to \
submit before you can graduate. You're current grade is {} and can increase \
to {} if you submit all assignments before the due date.\n\n"

for student_info in zip(names, assignments, grades):
    possible_grade = student_info[2] + student_info[1]*2
    formatted_message = message.format(*student_info, possible_grade)
    print(formatted_message)