import itertools as iter
import math

class Vector(object):
    def __init__(self, coordinates):
        try:
            if not coordinates:
                raise ValueError
            self.coordinates = tuple(coordinates)
            self.dimension = len(coordinates)
        except ValueError:
            raise ValueError('The coordinates must not be empty')
        except TypeError:
            raise TypeError('The coordinates must be an iterable')

    def __repr__(self):
        return 'Vector: {}'.format(self.coordinates)

    def __getitem__(self, index):
        return self.coordinates[index]


    def __eq__(self, other):
        return self.coordinates == other.coordinates

    def __add__(self, other):
        coordinates = [s + o for s, o in iter.zip_longest(self.coordinates, other.coordinates, fillvalue = 0)]
        return Vector(coordinates)

    def __sub__(self, other):
        coordinates = [s - o for s, o in iter.zip_longest(self.coordinates, other.coordinates, fillvalue = 0)]
        return Vector(coordinates)

    def __mul__(self, other):
        coordinates = [s * other for s in self.coordinates]
        return Vector(coordinates)

    def __rmul__(self, other):
        return self * other

    @property
    def is_zero_vector(self):
        nonzero_coords = [i for i in self.coordinates if round(i,4) != 0]
        return len(nonzero_coords) == 0


    @property
    def magnitude(self):
        coord_squares = [x ** 2 for x in self.coordinates]
        coord_squares_sum = math.fsum(coord_squares)
        return math.sqrt(coord_squares_sum)

    @property
    def unit_vector(self):
        try:
            return (1.0 / self.magnitude) * self

        except ZeroDivisionError:
            raise Exception('Cannot normalize the zero vector')
    
    @staticmethod
    def dot_product(v1, v2):
        if not (isinstance(v1, Vector) and isinstance(v2, Vector)):
            raise TypeError('Both arguments should be vectors')
        
        return math.fsum([a*b for a, b in zip(v1.coordinates, v2.coordinates)])

    @staticmethod
    def angle(v1, v2):
        if not (isinstance(v1, Vector) and isinstance(v2, Vector)):
            raise TypeError('Both arguments should be vectors')


        unit_vectors_dot_product = Vector.dot_product(v1.unit_vector, v2.unit_vector)

        return math.acos(round(unit_vectors_dot_product, 4))
    
    @staticmethod
    def are_parallel(v1, v2):
        try:
            if not (isinstance(v1, Vector) and isinstance(v2, Vector)):
                raise TypeError('Both arguments should be vectors')

            angle = Vector.angle(v1, v2)
            return abs(round(angle, 4)) == 0 or abs(round(angle, 4)) == round(math.pi, 4) or abs(round(angle, 4)) == round(2*math.pi, 4)

        except Exception as e:
            if str(e) == 'Cannot normalize the zero vector':
                return True
            else:
                return False

    @staticmethod
    def are_orthogonal(v1, v2):
        if not (isinstance(v1, Vector) and isinstance(v2, Vector)):
            raise TypeError('Both arguments should be vectors')

        return abs(round(Vector.dot_product(v1, v2))) == 0

    def project_onto(self, other):
        unit_vector_of_other = other.unit_vector
        return Vector.dot_product(self, unit_vector_of_other) * unit_vector_of_other

    def decompose_relative_to(self, other):
        parallel_to_other = self.project_onto(other)
        orthogonal_to_other = self - parallel_to_other
        return {'parallel': parallel_to_other, 'orthogonal': orthogonal_to_other}

    @staticmethod
    def cross_product(v1, v2):
        x_coordinate = v1[1]*v2[2] - v2[1]*v1[2]
        y_coordinate = -(v1[0]*v2[2] - v2[0]*v1[2])
        z_coordinate = v1[0]*v2[1] - v2[0]*v1[1]
        return Vector([x_coordinate, y_coordinate, z_coordinate])

    @staticmethod
    def get_paralellogram_area(v1, v2):
        return Vector.cross_product(v1, v2).magnitude 