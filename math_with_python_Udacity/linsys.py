from copy import deepcopy

from vector import Vector
from plane import Plane

class LinearSystem(object):

    ALL_PLANES_SHOULD_BE_SAME_DIM_MSG = 'All planes in the system should live in the same dimension'
    NO_SOLUTION_MSG = 'No solution'
    INF_SOLUTIONS_MSG = 'Infinitely many solutions'

    def __init__(self, planes):
        try:
            d = planes[0].dimension
            for p in planes:
                assert p.dimension == d

            self.planes = planes
            self.dimension = d

        except:
            raise Exception(self.ALL_PLANES_SHOULD_BE_SAME_DIM_MSG)


    def swap_rows(self, row1, row2):
        self[row1], self[row2] = self[row2], self[row1]


    def multiply_row_by_coefficient(self, coeff, row):
        target_normal = coeff * self[row].normal_vector
        target_constant = coeff * self[row].constant_term
        self[row] = Plane(target_normal, target_constant)
        

    def add_multiplied_row_to_row(self, coeff, row_to_be_added, target_row):
        n1 = self[target_row].normal_vector
        n2 = self[row_to_be_added].normal_vector
        k1 = self[target_row].constant_term
        k2 = self[row_to_be_added].constant_term

        target_normal = n1 + n2*coeff
        target_constant = k1 + k2*coeff

        self[target_row] = Plane(target_normal, target_constant)


    def compute_triangular_form(self):
        system = deepcopy(self)

        return system.compute_triangle_recursive(0)


    def compute_triangle_recursive(self, var_index):
        pivot_indices = self.indices_of_fist_nonzero_terms_in_each_row
        
        if pivot_indices.count(var_index) > 0:
            first_var_occurance = pivot_indices.index(var_index)
            if first_var_occurance != var_index:
                self.swap_rows(var_index, first_var_occurance)
            
            self.zero_variable(var_index, pivot_indices)

        var_index += 1
        if var_index == self.dimension or var_index == len(self.planes):
            return self
        else:
            return self.compute_triangle_recursive(var_index)


    def zero_variable(self, var_index, pivot_indices):
        for index in range(var_index + 1, len(pivot_indices)):
            if pivot_indices[index] == var_index:
                current_row_coeff = self[index].normal_vector[var_index]
                top_row_coeff = self[var_index].normal_vector[var_index]
                self.add_multiplied_row_to_row(-1*current_row_coeff/top_row_coeff, var_index, index)


    def compute_rref(self):
        system = self.compute_triangular_form()
        
        pivot_indices = system.indices_of_fist_nonzero_terms_in_each_row

        for i, pivot_index in enumerate(reversed(pivot_indices)):
            index = len(pivot_indices) - i - 1
            system.process_pivot_var(index, pivot_index)
            
        return system


    def process_pivot_var(self, index, pivot_index):
        if pivot_index != -1:
            pivot_coeff = self[index].normal_vector[pivot_index]
            self.multiply_row_by_coefficient(1 / pivot_coeff, index)

            for i in range(index):
                var_coeff = self[i].normal_vector[pivot_index]
                if round(var_coeff, 4) != 0:
                    self.add_multiplied_row_to_row(-1*var_coeff, index, i)

    
    def solve(self):
        rref = self.compute_rref()
        
        pivot_indices = rref.indices_of_fist_nonzero_terms_in_each_row

        for i, index in enumerate(pivot_indices):
            if index == -1 and round(rref[i].constant_term, 8) != 0:
                raise Exception('No solutions')

        worthy_equations_count = len([i for i in pivot_indices if i != -1])
        if worthy_equations_count < rref.dimension:
            return rref.parametrize()

        var_values = []
        for i in range(self.dimension):
            var_values.append(rref[i].constant_term)

        return Vector(var_values)


    def parametrize(self):
        dir_vectors = self.extract_dir_vectors()
        basepoint = self.extract_basepoint()

        return Parametrization(basepoint, dir_vectors)


    def extract_dir_vectors(self):
        pivot_indices = self.indices_of_fist_nonzero_terms_in_each_row
        free_var_indices = set(range(self.dimension)) - set(pivot_indices)
        dir_vectors = []
        print(pivot_indices)

        for free_var_index in free_var_indices:
            dir_vector_coords = [0]*self.dimension
            dir_vector_coords[free_var_index] = 1

            for i, plane in enumerate(self.planes):
                if pivot_indices[i] != -1:
                    dir_vector_coords[pivot_indices[i]] = -plane.normal_vector[free_var_index]

            dir_vectors.append(Vector(dir_vector_coords))

        return dir_vectors


    def extract_basepoint(self):
        pivot_indices = self.indices_of_fist_nonzero_terms_in_each_row
        basepoint_coords = [0]*self.dimension

        for i, plane in enumerate(self.planes):
            if pivot_indices[i] != -1:
                basepoint_coords[pivot_indices[i]] = plane.constant_term
        
        return Vector(basepoint_coords)


    @property
    def indices_of_fist_nonzero_terms_in_each_row(self):
        num_equations = len(self)
        num_variables = self.dimension

        indices = [-1] * num_equations

        for i, p in enumerate(self):
            try:
                indices[i] = p.first_nonzero_index(p.normal_vector)
            except Exception as e:
                if str(e) == Plane.NO_NONZERO_EL_FOUND_MSG:
                    continue
                else:
                    raise e

        return indices


    def __len__(self):
        return len(self.planes)


    def __getitem__(self, i):
        return self.planes[i]
        

    def __setitem__(self, i, value):
        try:
            assert value.dimension == self.dimension
            self.planes[i] = value

        except AssertionError:
            raise Exception(self.ALL_PLANES_SHOULD_BE_SAME_DIM_MSG)
            

    def __str__(self):
        ret = 'Linear System:\n'
        temp = [f'Equation {i + 1}: {p}' for i, p in enumerate(self.planes)]
        ret += '\n'.join(temp)
        return ret

    
class Parametrization(object):
    DIFFERENT_DIMS_MSG = (
        'The basepoint and direction vectors should all live in the same dimension')

    def __init__(self, basepoint, direction_vectors):
        self.basepoint = basepoint
        self.direction_vecotrs = direction_vectors
        self.dimension = self.basepoint.dimension

        try:
            for v in direction_vectors:
                assert v.dimension == self.dimension
        except AssertionError:
            raise Exception(self.DIFFERENT_DIMS_MSG)

    def __repr__(self):
        output = ""
        output += f"BasePoint: {str(self.basepoint)}"
        for vector in self.direction_vecotrs:
            output += f"\n{str(vector)}"

        return output